$(document).ready(function () {

    $('.slow-scroll').on('click', function (e) {
        e.preventDefault();
        $('.faq-list dl').show();
        $('.btn-more.all').hide();
        var block = $(this).attr('href');
        $('html, body').animate({
            scrollTop: $(block).offset().top - 70
        }, 400 )
    });

    $('.header-list ol .slow-scroll').on('click', function (e) {
        var block = $(this).attr('href');
        $(block).click();
    });

    // Accordion  js
    var allPanels = $('.faqAccordion > dd').hide(),
        allButtons =  $(".faqAccordion > dt > button").attr("aria-expanded", false);

    allButtons.on('click', function (e) {
        e.preventDefault();
        var $this = $(this),
            expanded = $this.attr('aria-expanded');

        if (expanded == "true") {
            $this.attr('aria-expanded', false);
            $this.parent().next().slideUp();
        } else {
            allPanels.slideUp();
            allButtons.attr('aria-expanded', false);
            $this.attr('aria-expanded', true);
            $this.parent().next().slideDown();
        }

    });
    $('.faqAccordion .btn-more ').on('click', function (e) {
        if(!$(this).hasClass('btn-more-less-main')){
            $(this).parents(".faqAccordion").find( "dt").fadeIn();
        }else{
            var all_answers = $(this).parents(".faqAccordion").find( "dt");
            for(var j=3;j < (all_answers.length - 1 );j++){
                all_answers.eq(j).fadeOut();
            }
        }
        $(this).toggleClass('btn-more-less-main');
    });
    $('.btn-more.all').on('click', function (e) {
        $(this).hide();
        $('.faq-list dl').show();

    });

    // error {"status":"fail","errors":["Error with shopify API"]}
    // paid {"status":"success","message":"Order is confirmed and yet to be shipped","financial_status":"paid","order_status_url":"https:\/\/www.thepetlabco.com\/1548746807\/orders\/93d62a5cfa96144af72756c8fa10996f\/authenticate?key=a069c2810c041eff5cf7acfc1f0b0a39"}
    // fulfilled {"status":"success","message":"Order is fulfilled","financial_status":"paid","order_status_url":"https:\/\/www.thepetlabco.com\/1548746807\/orders\/a2a3a2656ca3ffe4bf62dc5fa1c7610c\/authenticate?key=92c1f4b35427f69083da13f7d335b61a","order_fulfillment_status":"success","order_fulfillment_processed_at":"2020-06-03T23:11:17+01:00","order_fulfillment_tracking_company":"First Mile","order_fulfillment_tracking_number":"9261299990995425355245","order_fulfillment_tracking_url":"http:\/\/track.firstmile.com\/detail.php?n=9261299990995425355245"}
    //

    $('#tracking').validate();
    $(document).on('click', "#track", function (e) {

        if($('#tracking').valid()){
            e.preventDefault();
            $('.first-screen').hide();
            $('.result-box').show();
            $('.lds-default').show();
            $('.progress-content').hide();
            $('.traking-validation-error').hide();
            $('.progress-content-traking').hide();
            $('.track-line-wrap').hide();
            $('.product-wrap').hide();



            var  email = $('#email').val(),
                order = $('#order').val();

            $.ajax({
                type: "GET",
                url: " https://altitudereporting.net/orders_api/",
                contentType: "application/x-www-form-urlencoded",
                data: {
                    "email": email,
                    "brand" : 'balance-probiotic-skincare',
                    "order" : order,
                    "token" : '0106897791'
                },
                dataType: "json",
                success:function (response) {
                    $('.lds-default').hide();

                    if(response.message) {
                        var today = new Date();
                        var toDay = today.getDate();
                        var toMonth = today.getMonth();

                        var date = new Date(response.full_info.created_at);
                        var day = date.getDate();
                        var month = date.getMonth();
                        date = date.toGMTString();
                        var dateparts = date.split(' ');
                        var dmyDate = dateparts[0] + ' ' + dateparts[1] + ' ' + dateparts[2];
                        $('.track-line-wrap .ordered-date').text(dmyDate);

                        var trackLine = 0;
                        if (toDay == day && toMonth == month){
                            trackLine = 20;
                        } else {
                            trackLine = 20;
                        }
                        if(response.full_info.line_items) {
                            var outputProducts = '' ;
                            $.each(response.full_info.line_items, function(key, item) {
                                var productType = "";
                                console.log(item);
                               if (item.properties.length > 0) {
                                    var intervalType = "";
                                    var intervalFrequenc = "";
                                    $.each(item.properties, function(key, prop) {
                                        if (prop.name == "shipping_interval_frequency") {
                                            intervalFrequenc = "Every " + prop.value + " ";
                                        }
                                        if (prop.name == "shipping_interval_unit_type") {
                                            intervalType = prop.value + " ";
                                        }
                                    });

                                        productType = intervalFrequenc + intervalType + "Subscription";

                                } else if( item[0].product.tags.includes("Subscription") || item.name.includes("Off Auto renew")) {
                                    productType = "Subscription";
                                }
                                else {
                                    productType = "One-Time-Purchase";
                                }

                                outputProducts = outputProducts + '<li class="line-item"> <div class="img-wrap"><img src=' + item[0].product.image.src + ' alt=' + item.name + '></div>' ;
                                outputProducts = outputProducts + '<div class="content"><span class="quantity">' + item.quantity + '</span> <h4 class="name-product">' + item.name + '</h4></div>';
                                outputProducts = outputProducts + ' <p class="product-type">' +  productType + '</p></li>'
                            });
                        }

                        $('.product-list').html(outputProducts);
                        $('.progress-content-order').text('#' + order);
                        $('.progress-content-message').html(
                            '<a target="_blank" href="' + response.order_status_url + '" >'
                            + (response.message).replace('fulfilled','dispatched') +
                            '</a>'
                        );





                        if(response.order_fulfillment_tracking_number){

                            var dispatchedDate = new Date(response.order_fulfillment_processed_at);
                            var day = dispatchedDate.getDate();
                            var month = dispatchedDate.getMonth();
                            dispatchedDate = dispatchedDate.toGMTString();
                            var dispatchedDateParts = dispatchedDate.split(' ');


                            if (toDay == day && toMonth == month){
                                trackLine = 60;
                            } else {
                                trackLine = 80;
                            }

                            var dispatchedDmyDate = dispatchedDateParts[0] + ' ' + dispatchedDateParts[1] + ' ' + dispatchedDateParts[2];
                            $('.progress-content-traking-number').html('Tracking code: ' +
                               '<a target="_blank" href="' + response.order_fulfillment_tracking_url + '" >'
                                 +   response.order_fulfillment_tracking_number +
                                '</a>' );
                            $('.progress-content-traking').show();
                            $('.track-line-wrap .dispatched-date').text(dispatchedDmyDate);
                        }

                        $('.active-line').css('width', trackLine + '%');
                        $('.progress-content').show();
                        $('.track-line-wrap').show();
                        $('.product-wrap').show();

                    } else {
                        $('.traking-validation-error').show();
                    }

                },
                error: function (xhr, status) {
                    console.log(status);
                }
            });
        }
    });
    $(document).on('click', ".page-track-section .close", function (e) {
            e.preventDefault();
            $('.first-screen').show();
            $('.result-box').hide();
    });


    // Search
    $('.search__faq ').on('submit', function (e) {
      e.preventDefault();
        var searchTerm = $('.search__faq input ').val(),
         patt = new RegExp(searchTerm, "i"),
         find = 0;
        $("section.track").hide();
        $(".welcome-box").hide();
        $(".header-list").hide();
        $(".list-title").hide();
        $(".search__faq").hide();
        $(".search-result").show();
        $('.faq-list dt').show();
        $('.btn-more').hide();
        $('.faq-list').find('.faqAccordion').each(function() {
            var $table = $(this);
            if (!($(this).find('.faq--heading').text().search(patt) >= 0)) {
                var findTable = 0;
                $table.find('dd:hidden').each(function() {

                    if (!($(this).text().search(patt) >= 0)) {
                        if (!($(this).prev().text().search(patt) >= 0)){
                            $(this).prev().hide();
                        } else  {
                            findTable = 1;
                            find = 1;
                            $(this).prev().hide();
                        }
                    } else  {
                        findTable = 1;
                        find = 1;
                    }

                });

                if (findTable == 0) {
                    $table.hide();
                }
            } else {
                find = 1;
            }
        });

        if (find == 0) {
            $("#search-term").text(searchTerm);
            $(".search-error").show();
        }

    });
    $('.back-page ').on('click', function (e) {
        e.preventDefault();
        $("section.track").removeAttr("style");
        $(".welcome-box").removeAttr("style");
        $(".header-list").removeAttr("style");
        $(".list-title").removeAttr("style");
        $(".search__faq").removeAttr("style");
        $(".search-result").removeAttr("style");
        $('.faq-list .faqAccordion').removeAttr("style");
        $('.faqAccordion dt').removeAttr("style");
        $(".search-error").removeAttr("style");
        $('.btn-more').removeAttr("style");

    });


    if($(document).width() < 768){
        $('.h-list-product-container-desc').slick({
            centerMode: true,
            centerPadding: '30px',
            slidesToShow: 1,
            arrows: false,
        });
    }

});
