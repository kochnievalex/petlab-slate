import { register } from '@shopify/theme-sections';
import Swiper from 'swiper/bundle';

const selectors = {
  mainSlider: '[data-main-slider]'
};

register('slider', {
  async onLoad() { 
    this.slider = this.container.querySelector(selectors.mainSlider);
    
    this.initSlider(this.slider);
  },

  onUnload() {

  },

  initSlider(slider) {
    const slide = slider.querySelectorAll('.swiper-slide');
    const swiper = new Swiper(slider, { 
      slidesPerView: 1,
      spaceBetween: 0, 
      observer: true,
      observeParents: true,
      watchOverflow: true,
      autoplay: slide.length > 1 ? true : false,
      loop: slide.length > 1 ? true : false,
      autoplay: {
        delay: 4000,
        disableOnInteraction: true,
      },
      // pagination: {
      //   el: '.swiper-pagination',
      //   clickable: true,
      // },
    });
  },
});
