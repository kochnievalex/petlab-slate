import '../../styles/templates/index.scss';

import {load} from '@shopify/theme-sections';
import '../sections/home-slider';
import '../sections/home-review';

load('*');
