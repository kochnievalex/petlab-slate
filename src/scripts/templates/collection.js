import '../../styles/templates/collection.scss';
import $ from 'jquery';

const selectors = {
  collectionFilter: '[data-collection-filter]',
  collectionFilterToggle: '[data-collection-filter-toggle]',
  sort: '[data-sort]'
};


$(selectors.collectionFilterToggle).on('click', function(e){
  e.preventDefault();
  $(selectors.collectionFilter).toggleClass('active');
});

Shopify.queryParams = {};
  if (location.search.length) {
    for (var aKeyValue, i = 0, aCouples = location.search.substr(1)
      .split('&'); i < aCouples.length; i++) {
      aKeyValue = aCouples[i].split('=');
      if (aKeyValue.length > 1) {
        Shopify.queryParams[decodeURIComponent(aKeyValue[0])] = decodeURIComponent(aKeyValue[1]);
      }
    }
  }

  $('a[data-sort='+theme.sort+']').addClass('active');


  $('[data-sort]').on('click', function(e) {
      e.preventDefault();
      Shopify.queryParams.sort_by = $(this).attr('data-sort');
      location.search = $.param(Shopify.queryParams)
        .replace(/\+/g, '%20');
    });