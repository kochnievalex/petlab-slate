import $ from 'jquery';

const selectors = {
  header: '[data-header]',
  headerSearch: '[data-header-search]',
  headerSearchInput: '[data-header-search-input]',
  headerSearchClose: '[data-header-search-close]',
  headerNavButton: '[data-nav-button]',
  headerNav: '[data-header-nav]',
  headerNavLink: '[data-header-nav-link]'
}

$(selectors.headerNavButton).on('click', function() {
  $('body').toggleClass('ov-hidden'); 
  $(this).toggleClass('active');
  $(selectors.headerNav).toggleClass('active');
});
$(selectors.headerNavLink).on('click', function(e) {
  e.preventDefault();
  $(this).closest('li').toggleClass('active');
  $(this).next().slideToggle();
  $(this).closest('li').siblings().removeClass('active')
  $(this).closest('li').siblings().find(selectors.headerNavLink).next().slideUp()
  
})

$(selectors.headerSearchInput).on('focus', function() { 
  $(this).closest(selectors.headerSearch).addClass('active');
});
$(selectors.headerSearchClose).on('click', function() { 
  $(this).closest(selectors.headerSearch).removeClass('active');
});



$(window).on('scroll', function() {
  if ($(this).scrollTop() > 0) {
    $(selectors.header).addClass('sticky');
    $(selectors.header).parent().css('height', $(selectors.header).outerHeight());
  } else {
    $(selectors.header).removeClass('sticky');
    $(selectors.header).parent().css('height', 'auto');
  }
});