

jQuery.getJSON("/search/suggest.json", {
  "q": "jacket",
  "resources": {
    "type": "product",
    "limit": 5,
    "options": {
      "unavailable_products": "last",
      "fields": "title,product_type,variants.title"
    }
  }
}).done(function(response) {
  var productSuggestions = response.resources.results.products;

  if (productSuggestions.length > 0) {
    var firstProductSuggestion = productSuggestions[0];

    alert("The title of the first product suggestion is: " + firstProductSuggestion.title);
  }
});